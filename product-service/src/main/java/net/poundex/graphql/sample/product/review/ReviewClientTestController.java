package net.poundex.graphql.sample.product.review;

import net.poundex.graphql.sample.product.ProductServiceApplication.ReviewClient;
import lombok.RequiredArgsConstructor;
import net.poundex.graphql.sample.review.client.client.ReviewsGraphQLQuery;
import net.poundex.graphql.sample.review.client.client.ReviewsProjectionRoot;
import net.poundex.graphql.sample.review.client.types.Review;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/reviewclienttest")
@RequiredArgsConstructor
public class ReviewClientTestController {

    private final ReviewClient reviewClient;

    @GetMapping
    public Mono<ResponseEntity<List<Review>>> test() {
        return reviewClient.fetchList(Review.class,
                new ReviewsGraphQLQuery(),
                new ReviewsProjectionRoot().id().stars().review(), "reviews")
                .map(ResponseEntity::ok);
    }
}
