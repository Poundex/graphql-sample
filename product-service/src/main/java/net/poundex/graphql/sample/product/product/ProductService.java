package net.poundex.graphql.sample.product.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    
    public Flux<Product> findAll() {
        return productRepository.findAll();
    }

    public Mono<Product> findOne(String id) {
        return productRepository.findById(id);
    }
}
