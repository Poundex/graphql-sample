package net.poundex.graphql.sample.product;


import com.jayway.jsonpath.TypeRef;
import com.netflix.graphql.dgs.client.*;
import com.netflix.graphql.dgs.client.codegen.BaseProjectionNode;
import com.netflix.graphql.dgs.client.codegen.GraphQLQuery;
import com.netflix.graphql.dgs.client.codegen.GraphQLQueryRequest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.graphql.boot.GraphQlProperties;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootApplication
@EnableConfigurationProperties(GraphQlProperties.class)
public class ProductServiceApplication {

	public static void main(String[] args) {
		System.setProperty(
				"flogger.backend_factory", 
				"com.google.common.flogger.backend.slf4j.Slf4jBackendFactory#getInstance");
		
		SpringApplication.run(ProductServiceApplication.class, args);
	}
	
	public interface ReviewClient {
	    <T> Mono<T> fetchSingle(Class<T> klass, GraphQLQuery query, BaseProjectionNode projection, String path);
	    <T> Mono<List<T>> fetchList(Class<T> klass, GraphQLQuery query, BaseProjectionNode projection, String path);
	}
	
	@Bean
	public WebClient.Builder webClientBuilder() {
		return WebClient.builder();
	}

	@Bean
	public ReviewClient reviewClient(WebClient.Builder webClientBuilder) {
		WebClient webClient = webClientBuilder.baseUrl("http://localhost:8081").build();
		MonoGraphQLClient gqlClient = new DefaultGraphQLClient("/graphql");
		return new ReviewClient() {
			@Override
			public <T> Mono<T> fetchSingle(Class<T> klass, GraphQLQuery query, BaseProjectionNode projection, String path) {
				return executeQuery(query, projection)
						.map(r -> r.extractValueAsObject(path, klass));
			}

			@Override
			public <T> Mono<List<T>> fetchList(Class<T> klass, GraphQLQuery query, BaseProjectionNode projection, String path) {
				return executeQuery(query, projection)
						.map(r -> r.extractValueAsObject(path, new TypeRef<>() { } ));
//						.flatMapIterable(list -> list);
			}

			private Mono<GraphQLResponse> executeQuery(GraphQLQuery query, BaseProjectionNode projection) {
				GraphQLQueryRequest qlQueryRequest = new GraphQLQueryRequest(query, projection);
				return gqlClient.reactiveExecuteQuery(qlQueryRequest.serialize(), Map.of(), (url, headers, queryBody) ->
						webClient
								.post()
								.uri(url)
								.headers(h -> h.putAll(headers))
								.bodyValue(queryBody)
								.exchangeToMono(clientResponse -> clientResponse
										.bodyToMono(String.class)
										.map(respBody -> new HttpResponse(
												clientResponse.rawStatusCode(),
												respBody,
												clientResponse
														.headers()
														.asHttpHeaders()
														.entrySet()
														.stream()
														.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))))));
			}
		};
	}
}
