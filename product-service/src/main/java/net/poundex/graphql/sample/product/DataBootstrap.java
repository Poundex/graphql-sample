package net.poundex.graphql.sample.product;

import net.poundex.graphql.sample.product.product.Product;
import net.poundex.graphql.sample.product.product.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.flogger.Flogger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Profile("!mongoconfig")
@Component
@RequiredArgsConstructor
@Flogger
public class DataBootstrap implements ApplicationRunner {
    
    private final ProductRepository productRepository;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        Flux.just(
                Product.builder()
                        .id("PROD-RED-THING")
                        .name("Red Thing").description("A red thing").build(),
                Product.builder()
                        .id("PROD-GREEN-THING")
                        .name("Green Thing").description("A green thing").build(),
                Product.builder()
                        .id("PROD-BLUE-THING")
                        .name("Blue Thing").description("A blue thing").build(),
                Product.builder()
                        .id("PROD-GREY-THING")
                        .name("Grey Thing").description("No reviews for me")
                        .build())
                
                .flatMap(productRepository::save)
                .subscribe(t -> log.atInfo().log("Saved %s", t));
    }
}
