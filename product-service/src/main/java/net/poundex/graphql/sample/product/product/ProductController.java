package net.poundex.graphql.sample.product.product;

import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.GraphQlController;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import reactor.core.publisher.Flux;

@GraphQlController
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    
    @QueryMapping
    public Flux<Product> products() {
        return productService.findAll();
    }
}
