package net.poundex.graphql.sample.product.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Data
public class Product {
    @Id
    private String id;
    
    private String name;
    private String description;
}
