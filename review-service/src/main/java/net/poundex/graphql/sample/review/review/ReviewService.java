package net.poundex.graphql.sample.review.review;

import net.poundex.graphql.sample.review.product.Product;
import lombok.RequiredArgsConstructor;
import lombok.extern.flogger.Flogger;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Flogger
public class ReviewService {
    private final ReviewRepository reviewRepository;
    
    public Flux<Review> findAll() {
        return reviewRepository.findAll();
    }
    
    public Mono<Review> findOne(String id) {
        return reviewRepository.findById(id);
    }

    public Mono<Map<String, Product>> getProductsWithReviews(Set<String> keys) {
//        log.atWarning().log("DB QUERY! %s", keys);
        
        return reviewRepository.findAllByProductIn(keys)
                .groupBy(Review::getProduct)
                .flatMap(productId -> productId.collectList()
                        .map(listOfReviews -> new Product(productId.key(), listOfReviews)))
                .collectMap(Product::getId, v -> v);
    }
}
