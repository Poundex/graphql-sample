package net.poundex.graphql.sample.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReviewServiceApplication {

	public static void main(String[] args) {
		System.setProperty(
				"flogger.backend_factory",
				"com.google.common.flogger.backend.slf4j.Slf4jBackendFactory#getInstance");
		
		SpringApplication.run(ReviewServiceApplication.class, args);
	}

	
}
