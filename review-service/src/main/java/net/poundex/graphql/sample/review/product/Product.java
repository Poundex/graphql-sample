package net.poundex.graphql.sample.review.product;

import net.poundex.graphql.sample.review.review.Review;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Data
public class Product {
    private String id;
    
    private List<Review> reviews;
}
