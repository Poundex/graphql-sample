package net.poundex.graphql.sample.review.review;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

import java.util.Collection;

public interface ReviewRepository extends ReactiveMongoRepository<Review, String> {
    
    Flux<Review> findAllByProductIn(Collection<String> product);
}
