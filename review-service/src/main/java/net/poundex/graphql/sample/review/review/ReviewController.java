package net.poundex.graphql.sample.review.review;

import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.GraphQlController;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import reactor.core.publisher.Flux;

@GraphQlController
@RequiredArgsConstructor
public class ReviewController {
    private final ReviewService reviewService;
    
    @QueryMapping
    public Flux<Review> reviews() {
        return reviewService.findAll();
    }
}
