package net.poundex.graphql.sample.review.review;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Data
public class Review {
    @Id
    private String id;
    
    private int stars;
    private String review;
    
    private String product;
}
