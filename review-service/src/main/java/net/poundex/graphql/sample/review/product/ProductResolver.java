package net.poundex.graphql.sample.review.product;

import net.poundex.graphql.sample.review.review.ReviewService;
import lombok.RequiredArgsConstructor;
import net.poundex.graphql.spring.apollofederation.FederationReferenceResolver;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Set;

@RequiredArgsConstructor
@Component
public class ProductResolver implements FederationReferenceResolver<Product, String> {
    private final ReviewService reviewService;

    @Override
    public Mono<Map<String, Product>> resolveReferences(Set<String> keys) {
        return reviewService.getProductsWithReviews(keys);
    }

    @Override
    public String extractKey(Map<String, Object> args) {
        return (String) args.get("id");
    }
}
