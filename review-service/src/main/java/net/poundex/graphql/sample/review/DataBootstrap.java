package net.poundex.graphql.sample.review;

import net.poundex.graphql.sample.review.review.Review;
import net.poundex.graphql.sample.review.review.ReviewRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.flogger.Flogger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Profile("!mongoconfig")
@Component
@RequiredArgsConstructor
@Flogger
public class DataBootstrap implements ApplicationRunner {
    
    private final ReviewRepository reviewRepository;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        Flux.just(
                Review.builder().product("PROD-RED-THING").stars(3).review("Not bad, 3 stars").build(),
                Review.builder().product("PROD-BLUE-THING").stars(5).review("Best thing ever! 5 stars!!11!1one").build(),
                Review.builder().product("PROD-BLUE-THING").stars(4).review("Pretty good").build(),
                Review.builder().product("PROD-GREEN-THING").stars(0).review("Simply terrible. Zero stars").build())
                .flatMap(reviewRepository::save)
                .subscribe(t -> log.atInfo().log("Saved %s", t));
    }
}
